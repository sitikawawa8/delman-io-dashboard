// import styles from "./utils.module.css";
import Link from "next/link";
import React, { useState } from "react";
import {
  Flex,
  Heading,
  Avatar,
  AvatarGroup,
  Text,
  Icon,
  IconButton,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Divider,
  Box,
  Button,
  Input,
  InputGroup,
  InputLeftElement,
} from "@chakra-ui/react";
import {
  FiHome,
  FiPieChart,
  FiDollarSign,
  FiBox,
  FiCalendar,
  FiChevronDown,
  FiChevronUp,
  FiPlus,
  FiCreditCard,
  FiSearch,
  FiBell,
  FiUsers,
  FiUserPlus,
} from "react-icons/fi";

export default function RootLayout({ children }) {
  // const activeSegment = useSelectedLayoutSegment();
  const [display, changeDisplay] = useState("hide");
  const [value, changeValue] = useState(1);

  const links = [
    { label: "Dashboard", path: "/", targetSegment: null, icon: FiHome },
    { label: "Users", path: "/users", targetSegment: "users", icon: FiUsers },
    {
      label: "Registration",
      path: "/registration",
      targetSegment: "registration",
      icon: FiUserPlus,
    },
    {
      label: "Search",
      path: "/search",
      targetSegment: "search",
      icon: FiSearch,
    },
  ];

  return (
    <html>
      <head />
      <body>
        <Flex
          h={[null, null, "100vh"]}
          maxW="2000px"
          flexDir={["column", "column", "row"]}
          overflow="hidden"
        >
          {/* Column 1 */}
          <Flex
            w={["100%", "100%", "10%", "15%", "15%"]}
            flexDir="column"
            alignItems="center"
            backgroundColor="#020202"
            color="#fff"
          >
            <Flex
              flexDir="column"
              h={[null, null, "100vh"]}
              justifyContent="space-between"
            >
              <Flex flexDir="column" as="nav">
                <Heading
                  mt={50}
                  mb={[25, 50, 100]}
                  fontSize={["4xl", "4xl", "2xl", "3xl", "4xl"]}
                  alignSelf="center"
                  letterSpacing="tight"
                >
                  delman.io
                </Heading>
                <Flex
                  flexDir={["row", "row", "column", "column", "column"]}
                  align={[
                    "center",
                    "center",
                    "center",
                    "flex-start",
                    "flex-start",
                  ]}
                  wrap={["wrap", "wrap", "nowrap", "nowrap", "nowrap"]}
                  justifyContent="center"
                >
                  <div>
                    {links.map((items) => {
                      return (
                        <Flex className="sidebar-items" mr={[2, 6, 0, 0, 0]}>
                          <Link
                            key={items}
                            href={items.path}
                            display={["none", "none", "flex", "flex", "flex"]}
                          >
                            <Icon as={items.icon} fontSize="2xl" />
                          </Link>
                          <Link
                            key={items}
                            href={items.path}
                            _hover={{ textDecor: "none" }}
                            display={["flex", "flex", "none", "flex", "flex"]}
                          >
                            <Text>{items.label}</Text>
                          </Link>
                        </Flex>
                      );
                    })}
                  </div>
                </Flex>
              </Flex>
              <Flex flexDir="column" alignItems="center" mb={10} mt={5}>
                <Avatar my={2} src="avatar-1.jpg" />
                <Text textAlign="center">Muhammad Taqwa Aziz</Text>
              </Flex>
            </Flex>
          </Flex>

          {/* Column 2 */}
          <Flex
            w={["100%"]}
            p="3%"
            flexDir="column"
            overflow="auto"
            minH="100vh"
          >
            <div>{children}</div>
          </Flex>
        </Flex>
      </body>
    </html>
  );
}
