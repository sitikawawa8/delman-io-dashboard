import React, { useState } from "react";
import { Flex, Heading } from "@chakra-ui/react";

export default function Header({ title, subTitle }) {
  return (
    <Heading fontWeight="normal" mb={4} letterSpacing="tight">
      {title}
      <Flex display="inline-flex" fontWeight="bold">
        {subTitle}
      </Flex>
    </Heading>
  );
}
