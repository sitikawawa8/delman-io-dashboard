import React, { useState } from "react";
import {
  ChakraProvider,
  Box,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Input,
  Select,
} from "@chakra-ui/react";

const TableWithFilter = ({ renderTableBody, renderSearchInput }) => {
  return (
    <ChakraProvider>
      <Box p={4}>
        <Box mb={4}>{renderSearchInput}</Box>
        <Table variant="simple">
          <Thead>
            <Tr>
              <Th>id</Th>
              <Th>name</Th>
              <Th>sales_id</Th>
              <Th>item_id</Th>
              <Th>qty</Th>
              <Th>consumen_name</Th>
              <Th>transaction_date</Th>
            </Tr>
          </Thead>
          <Tbody>{renderTableBody}</Tbody>
        </Table>
      </Box>
    </ChakraProvider>
  );
};

export default TableWithFilter;
