import React, { useState } from "react";
import {
  Flex,
  Heading,
  Avatar,
  AvatarGroup,
  Text,
  Icon,
  IconButton,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Divider,
} from "@chakra-ui/react";
import { FiChevronDown, FiChevronUp } from "react-icons/fi";
import { useQuery } from "react-query";

const fetchData = async () => {
  const response = await fetch("https://delman-fe-api.fly.dev/users");
  if (!response.ok) {
    throw new Error("Network response was not ok");
  }
  return response.json();
};

export default function users() {
  const [display, changeDisplay] = useState("hide");
  const { data, error, isLoading } = useQuery("delmanData", fetchData);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {error.message}</p>;
  }
  return (
    <>
      <Text fontWeight="bold" fontSize="2xl">
        Users Data
      </Text>
      <Text color="blue" fontSize="sm">
        List of users data
      </Text>
      <Flex flexDir="column">
        <Flex overflow="auto">
          <Table variant="unstyled" mt={4}>
            <Thead>
              <Tr color="gray">
                <Th>id</Th>
                <Th>name</Th>
                <Th>email</Th>
                <Th>country_name</Th>
                <Th>device_id</Th>
                <Th>bitcoin_address</Th>
                <Th>avatar</Th>
                <Th>login_ip</Th>
                <Th>active_device_mac</Th>
                <Th>notes</Th>
                <Th>age</Th>
                <Th>referral_id</Th>
                <Th>locale</Th>
                <Th>favorite_music</Th>
                <Th>phone_number</Th>
                <Th>twitter_username</Th>
                <Th>job</Th>
                <Th>invoice_email_address</Th>
                <Th>hmac_secret</Th>
                <Th>favorite_quote</Th>
                <Th>primary_color</Th>
                <Th>secondary_color</Th>
                <Th>material</Th>
                <Th>shipping_address</Th>
                <Th>zip_code</Th>
                <Th>latitude</Th>
                <Th>longitude</Th>
                <Th>favorite_animal</Th>
                <Th>app_version</Th>
                <Th>timezone</Th>
              </Tr>
            </Thead>
            <Tbody>
              {data.data.map((items) => (
                <Tr>
                  <Td>{items.id}</Td>
                  <Td>{items.name}</Td>
                  <Td>{items.email}</Td>
                  <Td>{items.country_name}</Td>
                  <Td>{items.device_id}</Td>
                  <Td>{items.bitcoin_address}</Td>
                  <Td>{items.avatar}</Td>
                  <Td>{items.login_ip}</Td>
                  <Td>{items.active_device_mac}</Td>
                  <Td>{items.notes}</Td>
                  <Td>{items.age}</Td>
                  <Td>{items.referral_id}</Td>
                  <Td>{items.locale}</Td>
                  <Td>{items.favorite_music}</Td>
                  <Td>{items.phone_number}</Td>
                  <Td>{items.twitter_username}</Td>
                  <Td>{items.job}</Td>
                  <Td>{items.invoice_email_address}</Td>
                  <Td>{items.hmac_secret}</Td>
                  <Td>{items.favorite_quote}</Td>
                  <Td>{items.primary_color}</Td>
                  <Td>{items.secondary_color}</Td>
                  <Td>{items.material}</Td>
                  <Td>{items.shipping_address}</Td>
                  <Td>{items.zip_code}</Td>
                  <Td>{items.latitude}</Td>
                  <Td>{items.longitude}</Td>
                  <Td>{items.favorite_animal}</Td>
                  <Td>{items.app_version}</Td>
                  <Td>{items.timezone}</Td>
                </Tr>
              ))}

              {display == "show" && <></>}
            </Tbody>
          </Table>
        </Flex>
        <Flex align="center">
          <Divider />
          <IconButton
            icon={display == "show" ? <FiChevronUp /> : <FiChevronDown />}
            onClick={() => {
              if (display == "show") {
                changeDisplay("none");
              } else {
                changeDisplay("show");
              }
            }}
          />
          <Divider />
        </Flex>
      </Flex>
    </>
  );
}
