import React, { useState } from "react";
import { useQuery, useMutation } from "react-query";
import {
  Input,
  VStack,
  Heading,
  Box,
  Text,
  Spinner,
  Button,
} from "@chakra-ui/react";

const fetchUserByEmail = async (email) => {
  const response = await fetch(
    `https://delman-fe-api.fly.dev/users?email=${email}`
  );
  if (!response.ok) {
    throw new Error("User not found");
  }
  const userData = await response.json();
  return userData.length > 0 ? userData[0] : null;
};

const deleteUserById = async (id) => {
  const response = await fetch(`https://delman-fe-api.fly.dev/users/${id}`, {
    method: "DELETE",
  });
  if (!response.ok) {
    throw new Error("Failed to delete user");
  }
  return id;
};

const search = () => {
  const [userEmail, setUserEmail] = useState("");
  const { data, error, isLoading } = useQuery(
    ["userByEmail", userEmail],
    () => fetchUserByEmail(userEmail),
    {
      enabled: !!userEmail,
    }
  );

  const mutation = useMutation((id) => deleteUserById(id));

  const handleDelete = () => {
    mutation.mutate(data.id);
  };

  return (
    <VStack align="center" spacing="4">
      <Heading mb="4">Search User by Email</Heading>
      <Input
        placeholder="Enter user email"
        value={userEmail}
        onChange={(e) => setUserEmail(e.target.value)}
      />
      {isLoading && <Spinner size="lg" />}
      {error && <Text color="red">User not found</Text>}
      {data && (
        <Box mt="4">
          <Heading as="h3" size="md" mb="2">
            User Details
          </Heading>
          <Text>{`Name: ${data.name}`}</Text>
          <Text>{`Email: ${data.email}`}</Text>
          <Button
            colorScheme="red"
            mt="2"
            onClick={handleDelete}
            isLoading={mutation.isLoading}
          >
            Delete User
          </Button>
          {/* Add more details as needed */}
        </Box>
      )}
    </VStack>
  );
};

export default search;
