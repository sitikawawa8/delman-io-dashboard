import React, { useState } from "react";
import {
  Flex,
  Heading,
  Avatar,
  AvatarGroup,
  Text,
  Icon,
  IconButton,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Divider,
} from "@chakra-ui/react";
import { FiChevronDown, FiChevronUp } from "react-icons/fi";
import { useQuery } from "react-query";

const fetchData = async () => {
  const response = await fetch("https://delman-fe-api.fly.dev/");
  if (!response.ok) {
    throw new Error("Network response was not ok");
  }
  return response.json();
};

export default function Index() {
  const [display, changeDisplay] = useState("hide");
  const { data, error, isLoading } = useQuery("delmanData", fetchData);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {error.message}</p>;
  }
  return (
    <>
      <Text fontWeight="bold" fontSize="2xl">
        Sales Dashboard
      </Text>
      <Text color="blue" fontSize="sm">
        List of sales data
      </Text>
      <Flex flexDir="column">
        <Flex overflow="auto">
          <Table variant="unstyled" mt={4}>
            <Thead>
              <Tr color="gray">
                <Th>id</Th>
                <Th>name</Th>
                <Th>sales_id</Th>
                <Th>item_id</Th>
                <Th>qty</Th>
                <Th>consumen_name</Th>
                <Th>transaction_date</Th>
              </Tr>
            </Thead>
            <Tbody>
              {data.data.map((items) => (
                <Tr>
                  <Td>{items.id}</Td>
                  <Td>{items.name}</Td>
                  <Td>{items.sales_id}</Td>
                  <Td>{items.item_id}</Td>
                  <Td>{items.qty}</Td>
                  <Td>{items.consumen_name}</Td>
                  <Td>{items.transaction_date}</Td>
                </Tr>
              ))}

              {display == "show" && <></>}
            </Tbody>
          </Table>
        </Flex>
        <Flex align="center">
          <Divider />
          <IconButton
            icon={display == "show" ? <FiChevronUp /> : <FiChevronDown />}
            onClick={() => {
              if (display == "show") {
                changeDisplay("none");
              } else {
                changeDisplay("show");
              }
            }}
          />
          <Divider />
        </Flex>
      </Flex>
    </>
  );
}
